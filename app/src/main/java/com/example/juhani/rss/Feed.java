package com.example.juhani.rss;

//Feed item class. Holds variables inside myFeeds Array List

public class Feed{
    //VARIABLES
    private String headline;
    private String description;
    private int iconID;
    private String url;

    //CONSTRUCTOR
    public Feed(String headline, String description, int iconID, String url) {
        this.headline = headline;
        this.description = description;
        this.iconID=iconID;
        this.url=url;
    }
    //GETTERS
    public int getIconID(){return iconID;}
    public String getHeadline() {return headline;}
    public String getDescription() {return description;}
    public String getUrl() {
        return url;
    }




}