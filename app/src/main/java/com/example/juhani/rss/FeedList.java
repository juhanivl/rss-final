package com.example.juhani.rss;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Button;
import android.widget.ImageButton;
import java.util.ArrayList;
import java.util.List;

//FeedList class has the list view and adapter in inner class.
// also holds the pop up window for adding the feed.

public class FeedList extends Activity {
    private List<Feed> myFeeds = new ArrayList<Feed>();
    Button popup;
    Feed myFeed;
    ImageButton refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedlist);
        refresh = (ImageButton) findViewById(R.id.refresh);
        popup = (Button) findViewById(R.id.popup);

        final ArrayAdapter<Feed> adapter = new MyListAdapter();
        final ListView list = (ListView) findViewById(R.id.feedlistview);
        list.setAdapter(adapter);

        populateFeedList();
        populateListView();

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.clear();
                if(myFeed!=null) {
                    adapter.clear();
                    adapter.add(myFeed);
                    adapter.notifyDataSetChanged();
                    list.setAdapter(adapter);
                }
                populateFeedList();
                populateListView();
            }
        });

        popup.setOnClickListener(new View.OnClickListener(){
            @Override
        public void onClick(View v){
                final PopupWindow mpopup;
                View popUpView = getLayoutInflater().inflate(R.layout.addfeed, null); // inflating popup layout
                mpopup = new PopupWindow(popUpView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT, true); // Creation of popup
                mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
                mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0);// displaying popup

                final EditText title = (EditText) popUpView.findViewById(R.id.addtitletxt);
                final EditText description = (EditText) popUpView.findViewById(R.id.adddescriptiontxt);
                final EditText link = (EditText) popUpView.findViewById(R.id.addlinktxt);
                Button btnCancel = (Button) popUpView.findViewById(R.id.toList);
                Button btnadd = (Button) popUpView.findViewById(R.id.addFeed);

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mpopup.dismiss();
                    }
                });

                btnadd.setOnClickListener(new View.OnClickListener(){
                    @Override
                public void onClick(View v){
                        myFeed = new Feed(title.getText().toString(), description.getText().toString(),R.drawable.star, link.getText().toString());
                        myFeeds.add(myFeed);
                    }
                });
            }
        });
    }

    private void populateFeedList() {
        myFeeds.add(new Feed("Iltalehti", "Kotimaa", R.drawable.finland, "http://www.iltalehti.fi/rss/kotimaa.xml"));
        myFeeds.add(new Feed("Iltalehti", "Ulkomaa", R.drawable.maapallo,"http://www.iltalehti.fi/rss/ulkomaat.xml"));
        myFeeds.add(new Feed("Iltalehti", "Matkailu", R.drawable.travel, "http://www.iltalehti.fi/rss/matkailu.xml"));
        myFeeds.add(new Feed("Iltalehti", "Jääkiekko", R.drawable.hockey, "http://www.iltalehti.fi/rss/jaakiekko.xml"));
        myFeeds.add(new Feed("Iltalehti", "Musiikki", R.drawable.music, "http://www.iltalehti.fi/rss/musiikki.xml"));
        myFeeds.add(new Feed("Iltalehti", "Digi", R.drawable.computer, "http://www.iltalehti.fi/rss/digi.xml"));
        myFeeds.add(new Feed("Iltalehti", "Jalkapallo", R.drawable.football, "http://www.iltalehti.fi/rss/jalkapallo.xml"));
        myFeeds.add(new Feed("Pelaaja", "Pelaajalehden uutiset.", R.drawable.controller, "http://www.pelaajalehti.com/rss/uusimmat.xml"));
        myFeeds.add(new Feed("Yle", "Talous", R.drawable.finance, "http://yle.fi/uutiset/rss/uutiset.rss?osasto=talous"));
        myFeeds.add(new Feed("Yle", "Kulttuuri", R.drawable.culture, "http://yle.fi/uutiset/rss/uutiset.rss?osasto=kulttuuri"));
        myFeeds.add(new Feed("Yle", "Tiede", R.drawable.science, "http://yle.fi/uutiset/rss/uutiset.rss?osasto=tiede"));
        myFeeds.add(new Feed("Yle", "Merisää", R.drawable.ocean, "http://yle.fi/uutiset/rss/uutiset.rss?osasto=perameri"));
        myFeeds.add(new Feed("Yle", "Terveys", R.drawable.heart, "http://yle.fi/uutiset/rss/uutiset.rss?osasto=terveys"));
        myFeeds.add(new Feed("Yle", "Internet", R.drawable.wwww, "http://yle.fi/uutiset/rss/uutiset.rss?osasto=internet"));
        myFeeds.add(new Feed("Yle", "Luonto", R.drawable.oakleaf, "http://yle.fi/uutiset/rss/uutiset.rss?osasto=luonto"));
        myFeeds.add(new Feed("Yahoo", "Olympic", R.drawable.olympicrings, "https://sports.yahoo.com/oly/rss.xml"));
        myFeeds.add(new Feed("IMDB", "Celebrities born today", R.drawable.clappe, "http://rss.imdb.com/daily/born/"));
    }

    private void populateListView() {
        ArrayAdapter<Feed> adapter = new MyListAdapter();
        ListView list = (ListView) findViewById(R.id.feedlistview);
        list.setAdapter(adapter);
    }

    //INNER CLASS
    private class MyListAdapter extends ArrayAdapter<Feed> {
        //CONSTRUCTOR no need arguments because in inner class and we have access to all
        public MyListAdapter() {
            super(FeedList.this, R.layout.item, myFeeds); //WHAT VIEW WE WANT DISPLAYED ON THE LIST=ITE_VIEW AND WHAT CONTENT
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //MAKE SURE WE HAVE A VIEW TO WORK WITH
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.item, parent, false);
            }

            //FIND THE FEED TO WORK WITH
            Feed CurrentFeed = myFeeds.get(position);

            //IMAGE
            ImageView imageView = (ImageView) itemView.findViewById(R.id.item_icon);
            imageView.setImageResource(CurrentFeed.getIconID());

            //HEADLINE
            TextView HeadlineText = (TextView) itemView.findViewById(R.id.item_txtheadline);
            HeadlineText.setText(CurrentFeed.getHeadline());

            //DESCRIPTION
            TextView DescriptionText = (TextView) itemView.findViewById(R.id.item_txtdescription);
            DescriptionText.setText(CurrentFeed.getDescription());

            //LINK
            final TextView LinkText = (TextView) itemView.findViewById(R.id.item_txtlink);
            LinkText.setText(CurrentFeed.getUrl());
            //CREATE A STRING FROM THE LinkText and pass it to the FeedView class
            final String finalUrl=CurrentFeed.getUrl();

            //BUTTON
            Button buttonView = (Button) itemView.findViewById(R.id.item_button);
            buttonView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FeedList.this, FeedView.class);

                    Bundle b = new Bundle();
                    b.putString("url", finalUrl);
                    intent.putExtras(b);
                    startActivity(intent);
                }
            });

            return itemView;
        }
    }
}