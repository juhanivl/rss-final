package com.example.juhani.rss;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageButton;

//Individual FeedView class in which the user can read the whole news.
//Gets the link from FeedList and passes it forward to the parser HandleXML

public class FeedView extends Activity {
    private TextView title;
    private TextView description;
    private CheckBox check;
    private ImageButton next, previous, webBtn, refreshBtn;
    private HandleXML obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedview);

        title = (TextView) findViewById(R.id.headline);
        description = (TextView) findViewById(R.id.description);
        refreshBtn = (ImageButton) findViewById(R.id.button);
        webBtn = (ImageButton) findViewById(R.id.button2);
        //TODO: next ja previous näyttäis seuraavan ja edellisen uutisen.
        check = (CheckBox) findViewById(R.id.checkBox);
        next = (ImageButton) findViewById(R.id.next);
        previous = (ImageButton) findViewById(R.id.previous);

        //URL FROM THE FEEDLIST
        Bundle b = getIntent().getExtras();
        final String finalUrl = b.getString("url");
        //FETCH METHOD AFTER ONCREATE METHOD.
        fetch();

        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetch();
            }
        });

        //NEXT NEWS
        //TODO: only refreshes the news
        next.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                obj = new HandleXML(finalUrl);
                obj.fetchXML();

                while (obj.parsingComplete) ;
                title.setText(obj.getTitle());
                description.setText(obj.getDescription());
            }});

        //TO WEB VIEW
        webBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FeedView.this, WebView.class);

                Bundle b = new Bundle();
                String link = obj.getLink();
                ;
                b.putString("link", link);
                intent.putExtras(b);

                startActivity(intent);
            }
        });
    }

    public void fetch(){
        //URL FROM THE FEEDLIST
        Bundle b = getIntent().getExtras();
        final String finalUrl = b.getString("url");

        obj = new HandleXML(finalUrl);
        obj.fetchXML();

        while (obj.parsingComplete) ;
        title.setText(obj.getTitle());
        description.setText(obj.getDescription());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}








