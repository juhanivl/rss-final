package com.example.juhani.rss;

import android.app.Activity;
import android.os.Bundle;


import static com.example.juhani.rss.R.layout.webview;

public class WebView extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(webview);

        Bundle b = getIntent().getExtras();
        String link = b.getString("link");

        android.webkit.WebView w1=(android.webkit.WebView)findViewById(R.id.webView);
        w1.loadUrl(link);
    }
}
